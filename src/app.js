const mongoose = require("mongoose");
// const Customer = require("./models/Customer"); // #### If use Referencing ####
const Customer = require("./models/Customer").Customer; // #### If use Embedding ####
const Identifire = require("./models/Identifier");

// <<<<<<< Connection to MongoDB >>>>>>>
mongoose
  .connect("mongodb://localhost/mongo_db", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Successfully connect to MongoDB");
  })
  .catch((err) => {
    console.log("Connection error", err);
  });

// <<<<<<< Function to Create Customer >>>>>>>
const createCustomer = (name, age, gender) => {
  const customer = new Customer({
    name,
    age,
    gender,
  });

  return customer.save();
};

// <<<<<<< Function to Create Identifier >>>>>>>
const createIdentifier = (cardCode, customer) => {
  const identifier = new Identifire({
    cardCode,
    customer,
  });

  return identifier.save();
};

// <<<<<<< Try to Create Customer and Identifier >>>>>>>
createCustomer("Abdan", 25, "Male")
  .then((customer) => {
    console.log("> Created new Customer\n", customer);

    // #### If use Referencing ####
    //     const customerId = customer._id.toString();
    //     return createIdentifier(
    //       customerId.substring(0, 10).toUpperCase(),
    //       customerId
    //     );

    // #### If use Embedding ####
    return createIdentifier(
      customer._id.toString().substring(0, 10).toUpperCase(),
      customer
    );
  })
  .then((identifier) => {
    console.log("> Created new Identifier\n", identifier);
  })
  .catch((err) => console.log(err));

// <<<<<<< Show All Identifier >>>>>>>
// #### If use Referencing ####
// const showAllIdentifier = async () => {
//   // If to show all
//   const identifiers = await Identifire.find().populate("customer");

//   //   Mongoose Hide _id & __v in result
//   //   const identifiers = await Identifire.find().populate("customer", "-_id -__v");

//   //   If to hide __v in parent object
//   //   const identifiers = await Identifire.find()
//   //     .populate("customer", "-_id -__v")
//   //     .select("-__v");

//   console.log("> All Identifier\n", identifiers);
// };
// showAllIdentifier();

// #### If use Embedding ####
const showAllIdentifier = async () => {
  // If to show all
  //   const identifiers = await Identifire.find().populate("customer");

  //   Mongoose Hide _id & __v in result
  const identifiers = await Identifire.find().select(
    "-__v -customer.__v -customer._id"
  );

  console.log("> All Identifier\n", identifiers);
};
showAllIdentifier();
