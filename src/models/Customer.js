const mongoose = require("mongoose")

// Mongoose One-to-One relationship: Referencing
// const Customer = mongoose.model(
//   "Customer",
//   new mongoose.Schema({
//     name: String,
//     age: Number,
//     gender: String,
//   })
// );

// module.exports = Customer;

// Mongoose One-to-One relationship: Embedding
const CustomerSchema = new mongoose.Schema({
  name: String,
  age: Number,
  gender: String,
});

const Customer = mongoose.model("Customer", CustomerSchema);

module.exports = { Customer, CustomerSchema };
