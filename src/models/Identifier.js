const mongoose = require("mongoose");
const CustomerSchema = require("./Customer").CustomerSchema;

// Mongoose One-to-One relationship: Referencing
// const Identifire = mongoose.model(
//   "Identifier",
//   new mongoose.Schema({
//     cardCode: String,
//     customer: {
//       type: mongoose.Schema.Types.ObjectId,
//       ref: "Customer",
//     },
//   })
// );

// module.exports = Identifire;

// Mongoose One-to-One relationship: Embedding
const Identifier = mongoose.model(
  "Identifier",
  new mongoose.Schema({
    cardCode: String,
    customer: CustomerSchema,
  })
);

module.exports = Identifier;
